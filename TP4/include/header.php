<?php
/**
 * All global variable
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */
?>

<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
        <script src="https://kit.fontawesome.com/290c2e0fa2.js" crossorigin="anonymous"></script> <!-- personal api key -->
    </head>