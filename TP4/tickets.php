<?php
/**
 * Show ticker of a user
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

include "include/config.php";
include "include/header.php";

if(!isset($_GET["userid"])){
    include "include/HTTP_400.php";
    exit();
}

$userid = $_GET["userid"];

try {
    $dbh = new PDO("pgsql:host=$DB_URL;port=5432;dbname=etd", $DB_USERNAME, $DB_PASSWD);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Échec lors de la connexion : ' . $e->getMessage();
}

$req = "SELECT * FROM tickets WHERE utilisateur_id = $userid ORDER BY date DESC;";

$result = $dbh->query($req);
$tab = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<a href="https://pedago.univ-avignon.fr/~uapv2001785/TP4" class="w3-button" >Go back</a>
<button class="w3-button" onclick="$('td:nth-child(3)').toggle(500);$('th:nth-child(3)').toggle(500)">Hide product</button>

<table class="w3-table w3-striped">
    <tr>
      <th>ID</th>
      <th>Date</th>
      <th>Produit</th>
    </tr>
<?php

foreach($tab as $produits){

    $req = "SELECT ticket_entry.produit_id, ticket_entry.quantite, produits.nom, produits.categorie_id  FROM ticket_entry INNER JOIN produits ON ticket_entry.produit_id = produits.id WHERE ticket_id = ".$produits["id"]." ORDER BY produits.categorie_id;";

    $result = $dbh->query($req);
    $tickets = $result->fetchAll(PDO::FETCH_ASSOC);
    //var_dump( $tickets);
    echo "<tr>
        <td>". $produits["id"] ."</td>
        <td>". $produits["date"] ."</td>
        <td>";
        foreach($tickets as $tick){
            echo $tick["nom"] . " (" . $tick["quantite"] .")</br>";
        }
        
        echo "</td>
    </tr>";
}


?>