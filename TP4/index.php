<?php
/**
 * Index of TP4 (Same as utilisateur.php)
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


include "include/config.php";
include "include/header.php";

try {
    $dbh = new PDO("pgsql:host=$DB_URL;port=5432;dbname=etd", $DB_USERNAME, $DB_PASSWD);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Échec lors de la connexion : ' . $e->getMessage();

}
/*
 Requette N°1:
 $req = "SELECT produits.nom, categories.id FROM produits INNER JOIN categories ON produits.categorie_id = categories.id";
*/


/*
 Requette N°2:
 $req = "SELECT COUNT (ticket_entry.quantite), produits.nom FROM produits INNER JOIN ticket_entry ON ticket_entry.produit_id = produits.id GROUP BY produits.nom;";
*/

/*
 Requette N°3:
 $req = "SELECT tic.id, util.prenom, util.nom, sum(pro.prix * ticent.quantite) "total du ticket" FROM (
    tickets tic
    JOIN 
    utilisateurs util
    on (util.id = tic.utilisateur_id)
    JOIN 
    ticket_entry ticent
    on (tic.id = ticent.ticket_id)
    JOIN 
    produits pro
    on (pro.id = ticent.produit_id))
 GROUP BY tic.id, util.prenom, util.nom;" 
*/

/*
 Requette N°3:
 $req = "SELECT util.id, util.prenom, util.nom, sum(pro.prix * ticent.quantite) "total dépensé" FROM (
    tickets tic
    JOIN 
    utilisateurs util
    on (util.id = tic.utilisateur_id)
    JOIN 
    ticket_entry ticent
    on (tic.id = ticent.ticket_id)
    JOIN 
    produits pro
    on (pro.id = ticent.produit_id))
 GROUP BY util.id, util.prenom, util.nom
 ORDER BY util.id;" 
*/

 /*  
 Requette N°3:
 $req = "SELECT tic.date, sum(pro.prix * ticent.quantite) \"total encaissé\" FROM (
    tickets tic
    JOIN 
    utilisateurs util
    on (util.id = tic.utilisateur_id)
    JOIN 
    ticket_entry ticent
    on (tic.id = ticent.ticket_id)
    JOIN 
    produits pro
    on (pro.id = ticent.produit_id)
)
GROUP BY tic.date
ORDER BY tic.date;";*/


$req = "SELECT * FROM utilisateurs;";
?>

<table class="w3-table w3-striped">
    <tr>
      <th>ID</th>
      <th>Nom</th>
      <th>Prenom</th>
      <th>Profile</th>
    </tr>



<?php
$result = $dbh->query($req);
$tab = $result->fetchAll(PDO::FETCH_ASSOC);
foreach($tab as $produits){
    //var_dump($produits);
    echo"<tr>
        <td>".$produits["id"]."</td>
        <td>".$produits["nom"]."</td>
        <td>".$produits["prenom"]."</td>
        <td><a href=\"tickets.php?userid=".$produits["id"]."\">Profile of ".$produits["nom"]." </a></td>
    </tr>";
    //echo implode( " | ", $produits);
    //echo "</br>";
}
//var_dump($tab);
?>

