# TP1 PHP Cookies/sessions -  W3.CSS

Ce TP1 est le fruit du travaille de **Aubertin Emmanuel**

## Les comptes :

J'ai créé trois comptes pour que vous puissiez tester le site. Voici les accès à ces comptes :

| Nom d'utilisateur  | Mot de passe | Accréditation |
| ------------- | ------------- | ------------- |
| Manu  | MDP  | Root/Owner |
| Fabrice  | MDP  | User/Friend |
| Guest  | MDP  | Guest/Visitor |

## Où voir les fonctionnalités demandées ?

**1. Connexion**
    Pour se connecter, il suffit de cliquer sur l'icône de connexion dans le coin supérieur droit. Utilisez ensuite les identifiants figurant dans le tableau ci-dessus.
> Pour ajouter un utilisateur, vous devez modifier le tableau (`$user`) dans `include/config.php`.

**2. Contenu adapté**
    Tout d'abord l'utilisateur de niveau "Root" peut tout voir, pour l'instant il ne voit que la partie "Tarification" en plus des autres utilisateurs/groupes.
    Les utilisateurs du niveau "Friend" ont eux accès au bouton "See more".
    Pour les autres, ce connecter leur permet de modifier le thème du site.

**3. Modification de thème**
    Pour changer le thème, vous devez d'abord vous connecter. Une fois connecté, vous pouvez cliquer sur l'engrenage pour faire apparaître le modal des paramétres. Vous devrez ensuite choisir le thème et l'appliquer.


© Aubertin Emmanuel, 2022 / [athomisos.fr](https://athomisos.fr)