<?php
/**
 * Index of TP1
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

session_start();

include "include/config.php";

if(isset($_SESSION["username"])){
  $Login = TRUE;
}else{
  $Login = FALSE;
}

?>

<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $NAME  ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    <script src="https://kit.fontawesome.com/290c2e0fa2.js" crossorigin="anonymous"></script> <!-- personal api key -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <?php
    if($Login && isset($_COOKIE["theme"])){
      echo "<link rel=\"stylesheet\" href=\"https://www.w3schools.com/lib/w3-theme-".$_COOKIE["theme"].".css\">";
    }else{
      echo "<link rel=\"stylesheet\" href=\"https://www.w3schools.com/lib/w3-theme-amber.css\">";
    }
    ?>
    <style>
    body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
    body {font-size: 16px;}
    img {margin-bottom: -8px;}
    .mySlides {display: none;}
    </style>
  </head>
  <body class="w3-content w3-theme-d4 w3-card" style="max-width:1500px;">

  
  <header class="w3-container w3-xlarge w3-padding-24 w3-theme-d5">
    <a class="w3-left">Rowing</a>
    <?php
    if($Login){
      echo "<a onclick=\"document.getElementById('setting').style.display='block'\" class=\"w3-right w3-hover-opacity\"><i class=\"fa fa-gear \"></i></a><a href=\"logout.php\" class=\"w3-right  w3-button w3-hover-none w3-hover-opacity\">".$_SESSION["username"]." <i class=\"fa-solid fa-arrow-right-from-bracket\"></i></a>";
    }else{
      echo "<a onclick=\"document.getElementById('login').style.display='block'\" class=\"w3-right w3-hover-opacity\"><i class=\"fa fa-right-to-bracket \"></i></a>";
    }
    ?>
  </header>

  <!-- Header with Slideshow -->
  <header class="w3-display-container w3-center">
    <div class="mySlides w3-animate-opacity">
      <img class="w3-image" src="<?php echo $url_IMG_path?>sunset_rowing.jpeg" alt="Image 1" style="min-width:500px" width="1500" height="1000">
      <div class="w3-display-left w3-padding w3-hide-small" style="width:35%">
        <div class="w3-black w3-opacity w3-padding-large w3-round-large">
          <h1 class="w3-xlarge"></i><?php if($Login){echo "Hey " . $_SESSION["username"]; }else{ echo "Rowing"; }?></h1>
          <hr class="w3-opacity">
          <p>Feeling of freedom</p>
        </div>
      </div>
    </div>
    <div class="mySlides w3-animate-opacity">
      <img class="w3-image" src="<?php echo $url_IMG_path?>fourth.jpeg" alt="Image 2" style="min-width:500px" width="1500" height="1000">
      <div class="w3-display-left w3-padding w3-hide-small" style="width:35%">
        <div class="w3-black w3-opacity w3-padding-large w3-round-large">
          <h1 class="w3-xlarge"><b>Rowing</b></h1>
          <hr class="w3-opacity">
          <p>Moments for oneself</p>
        </div>
      </div>
    </div>
    <div class="mySlides w3-animate-opacity">
      <img class="w3-image" src="<?php echo $url_IMG_path?>height.jpeg" alt="Image 3" style="min-width:500px" width="1500" height="1000">
      <div class="w3-display-left w3-padding w3-hide-small" style="width:35%">
        <div class="w3-black w3-opacity  w3-padding-large w3-round-large">
          <h1 class="w3-xlarge">Rowing</h1>
          <hr class="w3-opacity">
          <p>Peaceful & calm</p>
     
        </div>
      </div>
    </div>
    <a class="w3-button w3-opacity w3-hover-opacity-off w3-black w3-display-right w3-margin-right w3-round w3-hide-small" onclick="plusDivs(1)"><i class="fa fa-angle-right"></i></a>
    <a class="w3-button w3-block w3-black w3-hide-large w3-hide-medium" onclick="plusDivs(1)">Next <i class="fa fa-angle-right"></i></a>
  </header>

  <!-- The App Section -->
  <div class="w3-padding-64 w3-white">
    <div class="w3-row-padding">
      <div class="w3-col l8 m6">
        <h1 class="w3-jumbo"><b>Rowing</b></h1>
        <h1 class="w3-xxxlarge w3-text-green"><b>Why practice it?</b></h1>
        <p><span class="w3-xlarge">Take photos like a pro.</span> You should buy this app because lorem ipsum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
          ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
        <button class="w3-button w3-light-grey w3-padding-large w3-section w3-hide-small" onclick="document.getElementById('more').style.display='block'">
          <i class="fa fa-eye"></i> See more
        </button> </div>
      <div class="w3-col l4 m6">
        <img src="<?php echo $url_IMG_path?>rowing-alone.jpeg" class="w3-image w3-right w3-hide-small w3-round" width="335" height="471">
      </div>
    </div>
  </div>

  <!-- 'See more' Modal -->
 <div id="more" class="w3-modal w3-animate-opacity">
    <div class="w3-modal-content" style="padding:32px">
      <div class="w3-container w3-white">
      <span onclick="document.getElementById('more').style.display='none'" 
      class="w3-button w3-display-topright">&times;</span>
      <!-- VISIBILITY : FRIEND & OWNER -->
        <?php
          if($Login && ($_SESSION["lvl"] == 0 || $_SESSION["lvl"] == 1)){ // SI ROOT OU FRIEND
            echo "<h2 class=\"w3-wide\">Hey you can read this because you'r my FRIEND</h2>";
          }else{
            echo "<h2 class=\"w3-wide\">PLZ LOGIN</h2>";
          }
        ?>
      </div>
    </div>
  </div>

  <!-- 'Settings' Modal -->
  <div id="setting" class="w3-modal w3-animate-opacity">
    <div class="w3-modal-content" style="padding:32px">
      <div class="w3-container w3-white">
      <span onclick="document.getElementById('setting').style.display='none'" 
      class="w3-button w3-display-topright">&times;</span>
      <!-- VISIBILITY : FRIEND & OWNER -->
        <?php
          if($Login && ($_SESSION["lvl"] == 0 || $_SESSION["lvl"] == 1)){ // SI ROOT OU FRIEND
            echo "<h2 class=\"w3-wide\">Settings :</h2>
            <h4>Themes :</h4>
            <select class=\"w3-select\" id=\"selectTheme\" name=\"selectTheme\">";
            foreach ($color_array as $color){
              echo "<option value=\"$color\">$color</option>";
            }
            echo "</select>
            <a class=\"w3-button w3-block w3-theme-d2 w3-padding\" onclick=\"applytheme()\">Apply</a>";
          }
        ?>
      </div>
    </div>
  </div>

  <!-- 'Login' Modal -->
  <div id="login" class="w3-modal w3-animate-opacity">
  <div class="w3-modal-content" style="padding:32px">
      <div class="w3-container w3-white">
        <span onclick="document.getElementById('login').style.display='none'" 
          class="w3-button w3-display-topright">&times;</span>
          <div id="loginerr">
            <h2> Login <h2>
          </div>
          <form class="w3-container" action="/action_page.php">
          
            <div class="w3-section">
              <div id="divusrname">
                <label><b>Username</b></label>
                <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Username" name="usrname" id="usrname">
              </div>
              <div id="divpsw">
                <label><b>Password</b></label>
                <input class="w3-input w3-border" type="password" placeholder="Enter Password" name="psw" id="psw">
              </div>
                <button class="w3-button w3-block w3-theme-d2 w3-section w3-padding" type="submit">Login</button>
            </div>
          </form>
      </div>
    </div>
  </div>
  
  <!-- Features Section -->
  <div class="w3-container w3-padding-64 w3-theme-l2 w3-center">
    <h1 class="w3-jumbo"><b>Features</b></h1>
    <p>This app is just so lorem ipsum.</p>

    <div class="w3-row" style="margin-top:64px">
      <div class="w3-col s3">
        <i class="fa fa-bolt w3-jumbo"></i>
        <p>Fast</p>
      </div>
      <div class="w3-col s3">
        <i class="fa fa-heart w3-jumbo"></i>
        <p>Loved</p>
      </div>
      <div class="w3-col s3">
        <i class="fa fa-camera w3-jumbo"></i>
        <p>Beautifull</p>
      </div>
      <div class="w3-col s3">
      <i class="fa fa-battery-full  w3-jumbo"></i>
        <p>Heathy</p>
      </div>
    </div>
  </div>

  <!-- Pricing Section 
        VISIBILITY : OWNER -->
<?php
  if($Login && $_SESSION["lvl"] == 0 ){ // If root
    echo "<div class=\"w3-padding-64 w3-center w3-white\">
    <h1 class=\"w3-jumbo\"><b>Pricing</b></h1>
    <p class=\"w3-large\">Choose a pricing plan that fits your needs.</p>
    <div class=\"w3-row-padding\" style=\"margin-top:64px\">
      <div class=\"w3-half w3-section\">
        <ul class=\"w3-ul w3-card w3-hover-shadow\">
          <li class=\"w3-dark-grey w3-xlarge w3-padding-32\">Basic</li>
          <li class=\"w3-padding-16\"><b>250</b> Photos</li>
          <li class=\"w3-padding-16\"><b>10</b> Features</li>
          <li class=\"w3-padding-16\"><b>No</b> Ads</li>
          <li class=\"w3-padding-16\"><b>Office hours</b> Support</li>
          <li class=\"w3-padding-16\">
            <h2 class=\"w3-opacity\">$ 25</h2>
          </li>
          <li class=\"w3-light-grey w3-padding-24\">
            <button class=\"w3-button w3-black w3-padding-large\" onclick=\"document.getElementById('more').style.display='block'\"><i class=\"fa fa-more\"></i> more</button>
          </li>
        </ul>
      </div>
      <div class=\"w3-half w3-section\">
        <ul class=\"w3-ul w3-card w3-hover-shadow\">
          <li class=\"w3-red w3-xlarge w3-padding-32\">Premium</li>
          <li class=\"w3-padding-16\"><b>1000</b> Photos</li>
          <li class=\"w3-padding-16\"><b>50</b> Features</li>
          <li class=\"w3-padding-16\"><b>No</b> Ads</li>
          <li class=\"w3-padding-16\"><b>Endless</b> Support</li>
          <li class=\"w3-padding-16\">
            <h2 class=\"w3-opacity\">$ 99</h2>
          </li>
          <li class=\"w3-light-grey w3-padding-24\">
            <button class=\"w3-button w3-black w3-padding-large\" onclick=\"document.getElementById('more').style.display='block'\"> <i class=\"fa fa-more\"></i> more</button>
          </li>
        </ul>
      </div>
    </div>
    <br>
  </div>";
  }else if($Login){
    echo "<p><span class=\"w3-text-red\">" .$_SESSION["username"] . "</span> is not in the sudoers file </p>";
  } else {
    echo "<p>Need to be login to see that :)</p>";
  }
?>
  <!-- Footer -->
  <footer class="w3-container w3-padding-32 w3-theme-d5 w3-center w3-xlarge">
    <div class="w3-section">
      <a href="https://gitlab.com/aTHO_" target="_blank"><i class="fa-brands fa-gitlab w3-hover-opacity"></i></a>
      <a href="https://github.com/Athomisos" target="_blank"><i class="fa-brands fa-github w3-hover-opacity"></i></a>
      <a href="https://e-uapv2021.univ-avignon.fr/user/profile.php?id=5513" target="_blank"><i class="fa fa-graduation-cap w3-hover-opacity"></i></a> <!-- <i class="fa-brands fa-github"></i> -->
      <a href="https://www.linkedin.com/in/emmanuel-aubertin/" target="_blank"><i class="fa fa-linkedin w3-hover-opacity"></i></a>
    </div>
    <p class="w3-medium"><?php echo $AUTHOR_html ?></p>
  </footer>

  <script>
  // Slideshow
  var slideIndex = 1;
  showDivs(slideIndex);

  function plusDivs(n) {
    showDivs(slideIndex += n);
  }

  function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    if (n > x.length) {slideIndex = 1}
    if (n < 1) {slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";  
    }
    x[slideIndex-1].style.display = "block";  
  }

  $(document).ready(function () {
  $("form").submit(function (event) {
    var formData = {
      usrname: $("#usrname").val(),
      psw: $("#psw").val(),
    };

    $.ajax({
      type: "POST",
      url: "login.php",
      data: formData,
      dataType: "json",
      encode: true,
    }).done(function (data) {
      if(!data.success){
        console.log("ERROR")
        if (data.errors.name) {
          $("#divusrname").append(
            '<p class="w3-text-red">' + data.errors.name + "</p>"
          );
        }
        if(data.errors.psw){
          $("#divpsw").append(
            '<p class="w3-text-red">' + data.errors.psw + "</p>"
          );
        }
        if(data.errors.login){
          $("#loginerr").append(
            '<p class="w3-text-red">' + data.errors.login + "</p>"
          );
        }
      } else {
        location.reload();
      }
      console.log(data);
    });

    event.preventDefault();
  });
});

  function applytheme(){
    var theme = document.getElementById("selectTheme").value;

    $("head").append("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/lib/w3-theme-"+theme+".css\">");
    document.cookie = "theme="+theme+";SameSite=secure";
  }
  </script>

  </body>
</html>