<?php
/**
 * Login Script 
 * Obviously => DO NOT USE IN PROD
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


include_once "include/config.php";

$status = TRUE;
$errors = [];
$data = [];

if ($_POST['usrname']== "") {
    $errors['name'] = 'Name is required.';
    $status = FALSE;
}

if ($_POST['psw']== "") {
    $errors['psw'] = 'Password is required.';
    $status = FALSE;
}

if($status){
    if($user[$_POST["usrname"]][1] == $_POST["psw"]){
        session_start();
        setcookie("PHPSESS", session_id());
        setcookie("username", $_POST["usrname"]);
        $_SESSION["username"] = $_POST["usrname"];
        $_SESSION["lvl"] = $user[$_POST["usrname"]][2];
        $data['success'] = true;
        $data['message'] = 'Success!';
    }else {
        $errors["login"] = "Name and password do not match !";
        $data['success'] = false;
        $data['errors'] = $errors;
    }
} else {
    $data['success'] = false;
    $data['errors'] = $errors;
}

echo json_encode($data);
 ?>