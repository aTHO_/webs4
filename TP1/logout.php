<?php
/**
 * Login Script 
 * Obviously => DO NOT USE IN PROD
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

session_start();
session_destroy();

?>
<script>
    window.location.href = "https://pedago.univ-avignon.fr/~uapv2001785/TP1";
</script>