<?php
/**
 * All global variable
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


/*-----| Chemin d'accéss |-----*/

    /*-- Pour les Url --*/                  $url_dir_path   =       "";
    /*-- Depuis la racine --*/              $dir_path       =       "/home/nas-wks01/users/uapv2001785/Donnees_itinerantes_depuis_serveur_pedagogique/public_html/TP4/";
    /*-- URL for IMG --*/                   $url_IMG_path   =       $url_dir_path .  "static/img/";
    /*-- URL for CSS --*/                   $url_CSS_path   =       $url_dir_path .  "static/css/";
    /*-- URL for JS --*/                    $url_JS_path    =       $url_dir_path .  "static/JS/";
    /*-- URL for font --*/                  $url_JS_path    =       $url_dir_path .  "static/font/";

/*-----| Project Var |-----*/
    /*-- Name --*/                          $NAME           =       "TP4";
    /*-- RELEASE --*/                       $RELEASE        =       "Revision 1.0";
    /*-- Author --*/                        $AUTHOR         =       "(c) 2021 Aubertin Emmanuel / https://athomisos.fr";
    /*-- Author with html --*/              $AUTHOR_html    =       "Powered by <a href=\"https://athomisos.fr\" target=\"_blank\" class=\"w3-hover-text-green\">Aubertin Emmanuel</a>";


/*-----| DB Var |-----*/
    /*-- DB url --*/                        $DB_URL         =       "192.168.2.13";
    /*-- DB user --*/                       $DB_USERNAME    =       "uapv2001785";
    /*-- DB psswd --*/                      $DB_PASSWD      =       "BexNnv";
    

    /*-- W3 Themes array --*/               $color_array    =       ["red", "pink", "purple", "deep-purple" , "indigo" , "blue" , "light-blue" , "cyan" , "teal" , "green" , "light-green" , "lime" , "khaki" , "yellow" , "amber" , "orange" , "deep-orange" , "blue-grey" , "brown" , "grey" , "dark-grey" , "black"]

?>