<?php
/**
 * Tiger class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

 include_once("Animal.php");


 class Tiger extends Animal {

    public function __construct($InputName){
        parent::__construct($InputName, "https://youtu.be/31CbrIqx5HA");
    }

    public function displaySound(){
        echo "<a href=\"".$this->SoundName."\" target=\"_blank\">Sound of ".$this->Name."</a>";
    }

    public function getIdentity(){
        return parent::getIdentity()."<h3>Type: Tiger</h3>";
    }
 }