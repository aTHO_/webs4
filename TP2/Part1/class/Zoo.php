<?php
/**
 * Zoo class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

 class Zoo {
     private $Name;
     private $Animales;

     function __construct($InputName){
        $this->Name = $InputName;
        $this->Animales = [];
     }

     public function addAnimal(Animal $InputAnimal)
     {
        /*if(is_subclass_of($InputAnimal, "Animal")) // A TYPÉ DANS LES ARG
        {*/
            array_push($this->Animales, $InputAnimal);
        /*} else 
        {
            echo "<h1>OH NO WRONG TYPE</h1>";
        }*/
     }

     public function nameAll()
     {
        $out = "";
        foreach($this->Animales as $Animale)
        {
            $current_info = $Animale->getInfo();
            $out .= "<p> Name : " . $current_info["Name"] . "</p></br>";
        }
        echo $out;
     }

    public function soundAll()
    {
        foreach($this->Animales as $Animal)
        {
            echo "<a href=\"".$Animal->getInfo()["SoundName"]."\"><a/>";
        }
    }

    public function displaySleeping($Animal)
    {
        if($Animal->isSleep)
        {
            return "<a class=\"w3-button\"><i class=\"fa fa-face-sleeping\"></a>";
        } else {
            return "<a class=\"w3-button\"><i class=\"fa fa-face-smile\"></a>";
        }
    }

    public function displayAll()
    {
        foreach($this->Animales as $Animal)
        {
            echo "<div class=\"w3-card w3-container w3-margin\">
            ".$Animal->getIdentity()."
            <a class=\"w3-button\" href=\"".$Animal->getInfo()["SoundName"]."\">Play sound</a>
            ".$this->displaySleeping($Animal)."
            </div>";
        }
    }

    public function getName()
    {
        return $this->Name;
    }
 }