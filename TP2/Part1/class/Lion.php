<?php
/**
 * Lion class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

 include_once("Animal.php");


 class Lion extends Animal {

    public function __construct($InputName){
        parent::__construct($InputName, "https://youtu.be/5rf4LGT_GMg");
    }

    public function displaySound(){
        echo "<a href=\"".$this->SoundName."\" target=\"_blank\">Sound of ".$this->Name."</a>";
    }

    public function getIdentity(){
        return parent::getIdentity()."<h3>Type: Lion</h3>";
    }
 }