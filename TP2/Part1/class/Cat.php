<?php
/**
 * Cat class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

 include_once("Animal.php");


 class Cat extends Animal {

    public function __construct($InputName){
        parent::__construct($InputName, "https://youtu.be/rj2WmCAluq4");
    }
    
    public function displaySound(){
        echo "<a href=\"".$this->SoundName."\" target=\"_blank\">Sound of ".$this->Name."</a>";
    }

    public function getIdentity(){
        return parent::getIdentity()."<h3>Type: Chat</h3>";
    }

 }