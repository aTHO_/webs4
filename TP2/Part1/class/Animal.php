<?php
/**
 * Animal class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

abstract class Animal{
    protected $Name;
    protected $SoundName;
    public $isSleep;

    public function __construct($InputName, $InputSound){
        $this->Name = $InputName;
        $this->SoundName = $InputSound;
        $this->isSleep = false;
    }

    public function getInfo(){
        return array("Name" => $this->Name, "SoundName" => $this->SoundName, "Sleep" => $this->isSleep);
    }

    public abstract function displaySound();
    
    public function getIdentity(){
        return "<h1>".$this->Name."</h1>";
    }

    public function sleep(){
        $this->isSleep = true;
    }
    
    public function wakeUp(){
        $this->isSleep = false;
    }
}