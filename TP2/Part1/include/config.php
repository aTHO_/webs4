<?php
/**
 * All global variable
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


/*-----| Chemin d'accéss |-----*/

    /*-- Pour les Url --*/                  $url_dir_path   =       "";
    /*-- Depuis la racine --*/              $dir_path       =       "/home/nas-wks01/users/uapv2001785/Donnees_itinerantes_depuis_serveur_pedagogique/public_html/";
    /*-- URL for IMG --*/                   $url_IMG_path   =       $url_dir_path .  "static/img/";
    /*-- URL for CSS --*/                   $url_CSS_path   =       $url_dir_path .  "static/css/";
    /*-- URL for JS --*/                    $url_JS_path    =       $url_dir_path .  "static/JS/";
    /*-- URL for font --*/                  $url_JS_path    =       $url_dir_path .  "static/font/";

/*-----| Project Var |-----*/
    /*-- Name --*/                          $NAME           =       "Zoo";
    /*-- RELEASE --*/                       $RELEASE        =       "Revision 1.0";
    /*-- Author --*/                        $AUTHOR         =       "(c) 2021 Aubertin Emmanuel / https://athomisos.fr";
    /*-- Author with html --*/              $AUTHOR_html    =       "Powered by <a href=\"https://athomisos.fr\" target=\"_blank\" class=\"w3-hover-text-green\">Aubertin Emmanuel</a>";


    $animelsType = ["Tiger", "Cat", "Lion"]
?>