<?php
/**
 * Zoo class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 **/

session_start();

include "include/header.php";
include_once "class/Zoo.php";
include_once "class/Tiger.php";
include_once "class/Lion.php";
include_once "class/Cat.php";

echo "<a href=\"logout.php\" class=\"w3-right  w3-button w3-hover-none w3-hover-opacity\">Delete <i class=\"fa-solid fa-trash\"></i></a>";


if(!isset($_SESSION["zoo"])){
    if(isset($_POST["zoo"])){
        $_SESSION = $_POST;
    } else {
        echo"<script>
    window.location.href = \"https://pedago.univ-avignon.fr/~uapv2001785/TP2/Part1/\";
</script>";
    }
}
    $MyZoo = new Zoo($_SESSION["zoo"]);
    $MyZooName = $MyZoo->getName();
    //echo "<h1> $MyZooName </h1>";
    //var_dump($_SESSION);
    for($i = 0; $i < intval($_SESSION["nbAni"]); $i++)
    {
        if(isset($_SESSION["type$i"]) && isset($_SESSION["Animale$i"]))
        {
            // need to add an "If in avai call array do" for prod
            $MyClass = $_SESSION["type$i"];
            $TempName = $_SESSION["Animale$i"];
            //echo "<p>new $MyClass(\"$TempName\")</p>"; // output => new Tigre("Tigrou")
            $TempObj = new $MyClass($TempName);
            //var_dump($TempObj);
            $MyZoo->addAnimal($TempObj);
        }
    }


echo "<h1>Bienvenue au Zoo $MyZooName </h1>";
$MyZoo->DisplayAll();

?>