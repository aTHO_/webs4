<?php
/**
 * Zoo class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


 include "include/config.php";
 include_once "class/Animal.php";
 include_once "class/Tiger.php";
 include_once "class/Lion.php";
 include_once "class/Cat.php";
 include_once "class/Zoo.php";



/*
$MyZoo = new Zoo("LaBarBaisse");

$Tiger = new Tiger("Bobby");
$Lion = new Lion("Fred");
$Cat = new Cat("Gisele");


$MyZoo->addAnimal($Tiger);
$MyZoo->addAnimal($Lion);
$MyZoo->addAnimal($Cat);

$MyZoo->nameAll();

*/

session_start();

if(!isset($_SESSION["zoo"])){


    function echoSelect($animelsType, $i){
        if($i != -1){
            echo "<select class=\"w3-select\" name=\"type". $i ."\"required>
            <option value=\"\" disabled selected>Choose your option</option>";
            foreach($animelsType as $Type){
                echo "<option value=\"". $Type ."\">$Type</option>";
            }   
            echo "</select>";
        } else {
            echo '<select class=\"w3-select\" name=\"type"+nbAni+"\" required><option value=\"\" disabled selected>Choose your option</option>';
            foreach($animelsType as $Type){
                echo '<option value=\"'.$Type.'\">'.$Type.'</option>';
            }   
            echo '</select>';
    }
    }   


echo "

    <html>
        <head>
            <title>$NAME</title>
            <meta charset=\"UTF-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">
            <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Poppins\">
            <script src=\"https://kit.fontawesome.com/290c2e0fa2.js\" crossorigin=\"anonymous\"></script> <!-- personal api key -->
            <script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js\"></script>
        </head>

        <body>
            <h1>Select your Zoo</h1>
            <form class=\"w3-container\" id=\"fromZoo\" action=\"zoo.php\" method=\"post\">
                <input type=\"hidden\" id=\"nbAni\" name=\"nbAni\" value=\"1\">
                <label>Zoo Name</label>
                <input class=\"w3-input\" type=\"text\" name=\"zoo\">
                <div id=\"addHere\">
                <div class=\"w3-card w3-container w3-margin\">
                    <div class=\"w3-margin\">";

                        // var_dump($animelsType);
                            echoSelect($animelsType, 0); 
echo"
                        <label>Animale Name</label>
                        <input class=\"w3-input\" type=\"text\" name=\"Animale0\" required>
                    </div>  
                </div>
                

                </div>
                <a class=\"w3-button\" onclick=\"addAnni();\">Add Animals</a>
                <button class=\"w3-button\">Submit</button>
            </form>

            <script type=\"text/javascript\">
                var nbAni = 1;

                function  addAnni(){
                    nbAni += 1;
                    document.getElementById(\"nbAni\").value = nbAni;
                    e = document.getElementById(\"addHere\");
                    e.insertAdjacentHTML('beforebegin', \"<div class='w3-card w3-container w3-margi'>\
                    <div class='w3-margin'>";

                            echoSelect($animelsType, -1); 
echo'<label>Animale Name</label>\
                        <input class=\"w3-input\" type=\"text\" name=\"Animale"+nbAni+"\" required>\
                    </div>\
                </div>");
                }
                
                function removeAni(id){
                    document.getElementById("nbAni").value = nbAni;
                    document.getElementById(id).remove();
                }

            </script>
        </body>
    </html>';
} else {
   echo  '<script>
   window.location.href = "https://pedago.univ-avignon.fr/~uapv2001785/TP2/Part1/zoo.php";
</script>';
}
?>