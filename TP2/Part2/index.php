<?php
/**
 * Zoo class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


 include "Colis.php";

 $MyColis = new Colis("Cheh Moux Ah");


/*
    Ici toutes les combinaisons sont possibles et fonctionnent car toutes les méthodes 
    de "Colis" retournent l'objet courant ($this), à l'exception du constructeur.
*/
$MyColis->getAdress()->setSubject("Hello World!")->getSubject();
  ?>