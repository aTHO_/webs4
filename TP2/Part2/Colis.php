<?php
/**
 * Zoo class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


/* IMPOSSIBLE CAR SORTIE EN PHP8 
(PEDAGO EST EN PHP7 EN 03/2022)
enum Status 
{
    case Pending;
    case Traveling;
    case WaitingExport;
    case Cancel;
}
*/

class Colis{
    private $adress;
    private $Subject;
    private $Content;
    private $Status;

    public function __construct($InputAdress){
        $this->adress = $InputAdress;
        return $this;
    }

    public function getAdress(){
        echo "<p>Adress : <em>" . $this->adress . "</em></p>";
        return $this;
    }

    public function setSubject($subject){
        $this->Subject = $subject;
        return $this;
    }

    public function getSubject(){
        echo "<p>Subject : <em>" . $this->Subject . "</em></p>";
        return $this;
    }

    public function setContent($Content){
        $this->Content = $Content;
        return $this;
    }

    public function addContent($Content){
        $this->Content += $Content;
        return $this;
    }

    public function sendLetter(){
        $this->Status = "Pending";
        return $this;
    }

    public function cancelLetter(){
        $this->Status = "Cancel";
        return $this;

    }

    public function getStatus(){
        echo $this->Status;
        return $this;
    }
}
?>