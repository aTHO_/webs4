<?php
/**
 * Class of sport Poste
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

abstract class Poste{
// J'ai choisie de prendre un class abstraite car je trouve le choix moins contraignent qu'une interface.
    protected $Name;
    protected $isFree;
    protected $operation;
    protected $Image;

    public function __construct($InputName, $InputImage){
        $this->Name = $InputName;
        $this->isFree = false;
        $this->Image = $InputImage;
    }

    public function showSelf(){
        echo "<div class=\"w3-card-4\">
        <img src=\"img_snowtops.jpg\" alt=\"Alps\">
        <div class=\"w3-container w3-center\">
            <h3>".$this->Name."</h3>
          <p>".$this->showStatue()."</p>
        </div>
      </div>";
    }


    public function showStatue(){ // Combi
        echo "<p>" .$this->Name . " est actuellement ";
        if($this->isFree){
            echo "disponible";
        } else {
            if($this->operation != ""){
                echo "indisponible car " . $this->operation . "en cours";
            } else {
                echo "indisponible car utilisation en cours";
            }
        }
        echo "</p>";
        return $this;
    }

    public function getInfo(){
        return array("Name" => $this->Name, "Free" => $this->isFree);
        return $this;
    }

    public function opOnPost($CurrentOpération)
    {
        $this->operation = $CurrentOpération;
        $this->isFree = false;
        return $this;
    }
    
    public function getIdentity(){
        return "<h1>".$this->Name."</h1>";
        return $this;
    }

    public function free(){ // => libéré
        if($this->isFree && $this->operation == ""){
            $this->isFree = false;
        } else {
            echo "Err poste not free";
        }
        return $this;
    }
    
    public function unFree(){ // => Aloué
        if(!$this->isFree){
            $this->isFree = true;
            $this->operation = "";
        } else {
            echo "Err can't free an free poste";
        }
        return $this;
    }
}

?>

<?php
/**
 * Class Rameur
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


 class Rameur extends Poste {
    private $marque;
    private $modele;

    public function __construct($InputName, $InputMarque, $InputModele){
        parent::__construct($InputName, "https://contents.mediadecathlon.com/p1780748/k$5e934b639818592082ad702c7443f1db/sq/rameur-d-pm5-concept-2.jpg?format=auto&f=969x969");
        $this->marque = $InputMarque;
        $this->modele = $InputModele;
    }

    public function getInfo(){
        $arr = parent::getInfo();
        $arr += ["Marque" => $this->marque, "Modele" => $this->modele];
        return $arr;
    }

 }
?>


<?php
/**
 * Class Banc
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


 class Banc extends Poste {
    private $marque;
    private $modele;

    public function __construct($InputName, $InputMarque, $InputModele){
        parent::__construct($InputName, "https://m.media-amazon.com/images/I/61ckTlseFWS._AC_SX425_.jpg");
        $this->marque = $InputMarque;
        $this->modele = $InputModele;
    }

    public function getInfo(){
        $arr = parent::getInfo();
        $arr += ["Marque" => $this->marque, "Modele" => $this->modele];
        return $arr;
    }

 }
?>

<?php
/**
 * Class TapisDeCourse
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


 class TapisDeCourse extends Poste {
    private $marque;
    private $modele;

    public function __construct($InputName, $InputMarque, $InputModele){
        parent::__construct($InputName, "https://www.cdiscount.com/pdt2/4/2/4/1/700x700/car3347265558424/rw/tapis-de-course-connecte-inclinable-25-programme.jpg");
        $this->marque = $InputMarque;
        $this->modele = $InputModele;
    }

    public function getInfo(){
        $arr = parent::getInfo();
        $arr += ["Marque" => $this->marque, "Modele" => $this->modele];
        return $arr;
    }

 }
?>