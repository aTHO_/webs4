<?php
/**
 * Zoo class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

 class Salle {
     private $Name;
     private $poste;

     function __construct($InputName){
        $this->Name = $InputName;
        $this->poste = [];
     }

     public function addPoste(Poste $InputPoste)
     {
        /*if(is_subclass_of($InputPoste, "Poste")) // A TYPÉ DANS LES ARG
        {*/
            array_push($this->poste, $InputPoste);
        /*} else 
        {
            echo "<h1>OH NO WRONG TYPE</h1>";
        }*/
     }

     public function nameAll()
     {
        $out = "";
        foreach($this->poste as $Postee)
        {
            $current_info = $Postee->getInfo();
            $out .= "<p> Name : " . $current_info["Name"] . "</p></br>";
        }
        echo $out;
     }

    public function soundAll()
    {
        foreach($this->poste as $Poste)
        {
            echo "<a href=\"".$Poste->getInfo()["SoundName"]."\"><a/>";
        }
    }

    public function displaySleeping($Poste)
    {
        if($Poste->isSleep)
        {
            return "<a class=\"w3-button\"><i class=\"fa fa-face-sleeping\"></a>";
        } else {
            return "<a class=\"w3-button\"><i class=\"fa fa-face-smile\"></a>";
        }
    }

    public function displayAll()
    {
        foreach($this->poste as $Poste)
        {
            echo "<div class=\"w3-card w3-container w3-margin\">
            ".$Poste->getIdentity()."
            <a class=\"w3-button\" href=\"".$Poste->getInfo()["SoundName"]."\">Play sound</a>
            ".$this->displaySleeping($Poste)."
            </div>";
        }
    }

    public function getName()
    {
        return $this->Name;
    }
 }
 ?>