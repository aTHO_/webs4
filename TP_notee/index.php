<?php
/**
 * All global variable
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


/*-----| Chemin d'accéss |-----*/

    /*-- Pour les Url --*/                  $url_dir_path   =       "";


/*-----| Project Var |-----*/
    /*-- Name --*/                          $NAME           =       "Speaure";
    /*-- RELEASE --*/                       $RELEASE        =       "Revision 1.0";
    /*-- Author --*/                        $AUTHOR         =       "(c) 2021 Aubertin Emmanuel / https://athomisos.fr";
    /*-- Author with html --*/              $AUTHOR_html    =       "Powered by <a href=\"https://athomisos.fr\" target=\"_blank\" class=\"w3-hover-text-green\">Aubertin Emmanuel</a>";

    /*-- Type of poste --*/                 $typeAvi        =       ["Rameur", "TapisDeCourse", "Banc"];


    session_start();
    if(isset($_GET["Destoy"])){
        unset($_SESSION);
        session_destroy();
    }



    if(isset($_POST["Salle"])){
        $NAME = $_POST["Salle"];
        $MySalle = new Salle($NAME);
    } else if(isset($_SESSION["Salle"])) {
        $NAME = $_SESSION["Salle"];
        $MySalle = new Salle($NAME);
    }

    if(isset($_SESSION["nbAni"])) {
        for($i = 0; $i < intval($_SESSION["nbAni"]); $i++) // INIT AVEC LES SESSIONS
        {
            if(isset($_SESSION["type$i"]) && isset($_SESSION["Poste$i"]))
            {
                // need to add an "If in avai call array do" for prod
                $MyClass = $_SESSION["type$i"];
                $TempName = $_SESSION["Poste$i"];
                //echo "<p>new $MyClass(\"$TempName\")</p>"; // output => new Tigre("Tigrou")
                $TempObj = new $MyClass($TempName);
                //var_dump($TempObj);
                $MySalle->addPoste($TempObj);
            }
        }
    }
    if(isset($_POST["nbAni"])) {
        for($i = 0; $i < intval($_POST["nbAni"]); $i++) // INIT AVEC LES SESSIONS
        {
            if(isset($_POST["type$i"]) && isset($_POST["Poste$i"]))
            {
                // need to add an "If in avai call array do" for prod
                $MyClass = $_POST["type$i"];
                $TempName = $_POST["Poste$i"];
                $_SESSION["Poste$i"] = $_POST["type$i"];
                $_SESSION["type$i"] = $_POST["Poste$i"];
                //echo "<p>new $MyClass(\"$TempName\")</p>"; // output => new Tigre("Tigrou")
                $TempObj = new $MyClass($TempName);
                //var_dump($TempObj);
                $MySalle->addPoste($TempObj);
            }
        }
    }

    if(isset($_GET["Unset"])){
        $MySalle->Unset($_GET["Unset"]);
    }

    if(isset($_GET["Unset"])){
        $MySalle->set($_GET["Unset"]);
    }

?>
<?php
/**
 * Class of sport Poste
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

abstract class Poste{
// J'ai choisie de prendre un class abstraite car je trouve le choix moins contraignent qu'une interface.
    protected $Name;
    protected $isFree;
    protected $operation;
    protected $Image;

    public function __construct($InputName, $InputImage){
        $this->Name = $InputName;
        $this->isFree = true;
        $this->Image = $InputImage;
    }

    public function showSelf(){
        /*echo "<div class=\"w3-card-4\">
        <img src=\"".$this->Image."\" alt=\"Alps\">
        <div class=\"w3-container w3-center\">
            <h3>".$this->Name."</h3>
          <p>". $this->getStatue() ."</p>
        </div>
      </div>";*/

      echo "<tr>
      <td>".$this->Type()."</td>
      <td>".$this->Name."</td>
      <td>".$this->getStatue()."</td>
      <td><a href=\"index.php?Unset=".$this->Name."\" class=\"w3-right  w3-button w3-hover-none w3-hover-opacity\">Delete <i class=\"fa-solid fa-trash\"></i></a></td>
      <td><a href=\"index.php?set=".$this->Name."\" class=\"w3-right  w3-button w3-hover-none w3-hover-opacity\">Delete <i class=\"fa-solid fa-trash\"></i></a></td>
    </tr>";
    }

    abstract function Type();

    private function getStatue(){ // Combi
        $out = "<p>" .$this->Name . " est actuellement ";
        if($this->isFree){
            $out .= "disponible";
        } else {
            if($this->operation != ""){
                $out.= "indisponible car " . $this->operation . "en cours";
            } else {
                $out .="indisponible car utilisation en cours";
            }
        }
        $out .="</p>";
        return $out;
    }

    public function showStatue(){ // Combi
        echo "<p>" .$this->Name . " est actuellement ";
        if($this->isFree){
            echo "disponible";
        } else {
            if($this->operation != ""){
                echo "indisponible car " . $this->operation . "en cours";
            } else {
                echo "indisponible car utilisation en cours";
            }
        }
        echo "</p>";
        return $this;
    }

    public function getInfo(){
        return array("Name" => $this->Name, "Free" => $this->isFree);
        return $this;
    }

    public function opOnPost($CurrentOpération)
    {
        $this->operation = $CurrentOpération;
        $this->isFree = false;
        return $this;
    }
    
    public function getIdentity(){
        return "<h1>".$this->Name."</h1>";
        return $this;
    }

    public function free(){ // => libéré
        if($this->isFree && $this->operation == ""){
            $this->isFree = false;
        } else {
            echo "Err poste not free";
        }
        return $this;
    }
    
    public function unFree(){ // => Aloué
        if(!$this->isFree){
            $this->isFree = true;
            $this->operation = "";
        } else {
            echo "Err can't free an free poste";
        }
        return $this;
    }
}

?>

<?php
/**
 * Class Rameur
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


 class Rameur extends Poste {

    public function __construct($InputName){
        parent::__construct($InputName, "https://contents.mediadecathlon.com/p1780748/k$5e934b639818592082ad702c7443f1db/sq/rameur-d-pm5-concept-2.jpg?format=auto&f=969x969");
    }

    public function getInfo(){
        $arr = parent::getInfo();
        $arr += ["Marque" => $this->marque, "Modele" => $this->modele];
        return $arr;
    }

    public function Type(){
        return "Rameur";
    }

 }
?>


<?php
/**
 * Class Banc
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


 class Banc extends Poste {

    public function __construct($InputName){
        parent::__construct($InputName, "https://m.media-amazon.com/images/I/61ckTlseFWS._AC_SX425_.jpg");
    }

    public function getInfo(){
        $arr = parent::getInfo();
        $arr += ["Marque" => $this->marque, "Modele" => $this->modele];
        return $arr;
    }
    public function Type(){
        return "Banc";
    }
 }
?>

<?php
/**
 * Class TapisDeCourse
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */


 class TapisDeCourse extends Poste {

    public function __construct($InputName){
        parent::__construct($InputName, "https://www.cdiscount.com/pdt2/4/2/4/1/700x700/car3347265558424/rw/tapis-de-course-connecte-inclinable-25-programme.jpg");
    }

    public function getInfo(){
        $arr = parent::getInfo();
        $arr += ["Marque" => $this->marque, "Modele" => $this->modele];
        return $arr;
    }
    public function Type(){
        return "Tapis de course";
    }
 }
?>
<?php
/**
 * Zoo class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

 class Salle {
     private $Name;
     private $poste;

     function __construct($InputName){
        $this->Name = $InputName;
        $this->poste = [];
     }

     public function Unset($InputName){
         foreach($this->poste as $Poste){
             if($Poste->Name == $InputName){
                 $Poste->unFree();
             }
         }
     }

     public function set($InputName){
        foreach($this->poste as $Poste){
            if($Poste->Name == $InputName){
                $Poste->free();
            }
        }
    }

     public function addPoste(Poste $InputPoste)
     {
            array_push($this->poste, $InputPoste);
     }

     public function nameAll()
     {
        $out = "";
        foreach($this->poste as $Postee)
        {
            $current_info = $Postee->getInfo();
            $out .= "<p> Name : " . $current_info["Name"] . "</p></br>";
        }
        echo $out;
     }

    public function soundAll()
    {
        foreach($this->poste as $Poste)
        {
            echo "<a href=\"".$Poste->getInfo()["SoundName"]."\"><a/>";
        }
    }


    public function displayAll()
    {
        echo "<table class=\"w3-table\">
        <tr>
          <th>Type</th>
          <th>Name</th>
          <th>Status</th>
          <th>Unfree</th>
          <th>Free</th>
        </tr>";
        foreach($this->poste as $Poste)
        {
            $Poste->showSelf();
        }
        echo "</table>";
    }

    public function getName()
    {
        return $this->Name;
    }
 }
 ?> 
<?php
/**
 * Test of Part2
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
**/
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $NAME ?></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-bar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
</style>
</head>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-red w3-card w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large w3-white">Home</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">Link 1</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">Link 2</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">Link 3</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">Link 4</a>
  </div>

  <!-- Navbar on small screens -->
  <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large">
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 1</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 2</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 3</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 4</a>
  </div>
</div>

<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:128px 16px">
  <h1 class="w3-margin w3-jumbo"><?php echo $NAME ?></h1>
  <p class="w3-xlarge">Le salle faite pour vous</p>
  <button class="w3-button w3-black w3-padding-large w3-large w3-margin-top">Get Started</button>
</header>

<!-- First Grid -->
<?php
/**
 * Salle class
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */







function echoSelect($typeAvi, $i){
    if($i != -1){
        echo "<select class=\"w3-select\" name=\"type". $i ."\"required>
        <option value=\"\" disabled selected>Choose your option</option>";
        foreach($typeAvi as $Type){
            echo "<option value=\"". $Type ."\">$Type</option>";
        }   
        echo "</select>";
    } else {
        echo '<select class=\"w3-select\" name=\"type"+nbAni+"\" required><option value=\"\" disabled selected>Choose your option</option>';
        foreach($typeAvi as $Type){
            echo '<option value=\"'.$Type.'\">'.$Type.'</option>';
        }   
        echo '</select>';
    }
}   


echo "
            <h1>Select your Salle</h1>
            <form class=\"w3-container\" id=\"fromSalle\" action=\"index.php\" method=\"post\">
                <input type=\"hidden\" id=\"nbAni\" name=\"nbAni\" value=\"1\">
                <label>Salle Name</label>
                <input class=\"w3-input\" type=\"text\" name=\"Salle\">
                <div id=\"addHere\">
                <div class=\"w3-card w3-container w3-margin\">
                    <div class=\"w3-margin\">";

                        // var_dump($typeAvi);
                            echoSelect($typeAvi, 0); 
echo"
                        <label>Poste Name</label>
                        <input class=\"w3-input\" type=\"text\" name=\"Poste0\" required>
                    </div>  
                </div>
                

                </div>
                <a class=\"w3-button\" onclick=\"addAnni();\">Add Animals</a>
                <button class=\"w3-button\">Submit</button>
            </form>

            <script type=\"text/javascript\">
                var nbAni = 1;

                function  addAnni(){
                    nbAni += 1;
                    document.getElementById(\"nbAni\").value = nbAni;
                    e = document.getElementById(\"addHere\");
                    e.insertAdjacentHTML('beforebegin', \"<div class='w3-card w3-container w3-margi'>\
                    <div class='w3-margin'>";

                            echoSelect($typeAvi, -1); 
echo'<label>Poste Name</label>\
                        <input class=\"w3-input\" type=\"text\" name=\"Poste"+nbAni+"\" required>\
                    </div>\
                </div>");
                }
                
                function removeAni(id){
                    document.getElementById("nbAni").value = nbAni;
                    document.getElementById(id).remove();
                }

            </script>';
            echo "<a href=\"index.php?Destroy=True\" class=\"w3-right  w3-button w3-hover-none w3-hover-opacity\">Delete <i class=\"fa-solid fa-trash\"></i></a>";

?>

<!-- Second Grid -->
<div class="w3-row-padding w3-light-grey w3-padding-64 w3-container">
  <div class="w3-content">
    <?php
    if(isset($MySalle)){
        echo "<h1>Bienvenue dans la salle ".$MySalle->getName()." </h1>";
        /*var_dump($MySalle);
        var_dump($_SESSION);
        var_dump($_POST);*/
            $MySalle->displayAll();
    }
    ?>

  </div>
</div>

<div class="w3-container w3-black w3-center w3-opacity w3-padding-64">
    <h1 class="w3-margin w3-xlarge">Quote of the day: live life</h1>
</div>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">  
  <div class="w3-xlarge w3-padding-32">
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
 </div>
 <p><?php echo $AUTHOR_html ?></p>
</footer>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>

</body>
</html>

<?php
/**
 * Test of Part1
 * 
 * @author Aubertin Emmanuel | https://athomisos.fr
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 

 include_once "class/Poste.php";

 $MyPost = new Rameur("R1", "Concept2", "PM5");

 $MyPost->opOnPost("Réparation")->showStatue();
 
 $MyPost->unFree()->showStatue(); // liberation aprés reparation

 $MyPost->free()->showStatue()->opOnPost("Maintenance")->showStatue();
 */?>